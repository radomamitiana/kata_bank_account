package com.bank.api.service;

import com.bank.api.data.dto.AccountDTO;
import com.bank.api.exceptions.ObjectNotFoundException;
import com.bank.api.repository.AccountRepository;
import com.bank.api.services.account.AccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountServiceTests {
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testCreateAccount() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(500.0));

        accountDTO = accountService.create(accountDTO);
        Assertions.assertNotNull(accountDTO.getId());
    }

    @Test
    public void testDeleteAccount() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(500.0));

        accountDTO = accountService.create(accountDTO);
        Boolean isDeleted = accountService.deleteAccount(accountDTO.getId());
        Assertions.assertEquals(isDeleted, Boolean.TRUE);
    }

    @Test
    public void testGetAccountById_OK() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(500.0));

        accountDTO = accountService.create(accountDTO);
        AccountDTO resultAccountDTO = accountService.getAccountById(accountDTO.getId());
        Assertions.assertNotNull(resultAccountDTO);
        Assertions.assertNotNull(resultAccountDTO.getId());
    }

    @Test
    public void testGetAccountById_NOT_FOUND() {
        Throwable throwable = Assertions.assertThrows(Throwable.class, () -> {
            accountService.getAccountById(123456789L);
        });
        Assertions.assertEquals(ObjectNotFoundException.class, throwable.getClass());
    }

    @Test
    public void testGetAllAccount() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(500.0));

        accountService.create(accountDTO);

        accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Toto");
        accountDTO.setBalance(BigDecimal.valueOf(500.0));

        accountService.create(accountDTO);

        List<AccountDTO> resultAccountDTO = accountService.getAllAccount();
        Assertions.assertNotNull(resultAccountDTO);
        Assertions.assertEquals(resultAccountDTO.size() > 0, Boolean.TRUE);
    }

}
