package com.bank.api.service;

import com.bank.api.data.dto.AccountDTO;
import com.bank.api.data.dto.HistoryDTO;
import com.bank.api.data.dto.OperationDTO;
import com.bank.api.exceptions.FunctionalInvalidDataException;
import com.bank.api.repository.AccountRepository;
import com.bank.api.services.account.AccountService;
import com.bank.api.services.operation.OperationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OperationServiceTests {
    @Autowired
    private AccountService accountService;

    @Autowired
    private OperationService operationService;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testDeposit() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(1000.0));

        operationDTO = operationService.deposit(operationDTO);

        Assertions.assertNotNull(operationDTO);
        Assertions.assertNotNull(operationDTO.getId());
    }

    @Test
    public void testWithDrawalOk() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(1000.0));

        operationDTO = operationService.withdrawal(operationDTO);

        Assertions.assertNotNull(operationDTO);
        Assertions.assertNotNull(operationDTO.getId());
    }

    @Test
    public void testWithDrawal_No_Ok() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(11000.0));

        Throwable throwable = Assertions.assertThrows(Throwable.class, () -> {
            operationService.withdrawal(operationDTO);
        });
        Assertions.assertEquals(FunctionalInvalidDataException.class, throwable.getClass());
    }

    @Test
    public void testGetMyOperations() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(11000.0));

        operationService.deposit(operationDTO);

        operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(11000.0));

        operationService.deposit(operationDTO);

        List<HistoryDTO> historyDTOList = operationService.getMyOperations(accountDTO.getId());

        Assertions.assertNotNull(historyDTOList);
        Assertions.assertEquals(historyDTOList.size(), 2);
    }
}
