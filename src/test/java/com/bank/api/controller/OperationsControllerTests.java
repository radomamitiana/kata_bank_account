package com.bank.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bank.api.data.dto.AccountDTO;
import com.bank.api.data.dto.OperationDTO;
import com.bank.api.services.account.AccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class OperationsControllerTests {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AccountService accountService;

    private String getRootApiOperationUrl() {
        return "http://localhost:" + port + "/api/operation";
    }

    @Test
    public void testGetMyOperations() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        ResponseEntity<String> response = restTemplate.exchange(getRootApiOperationUrl() + "/" + accountDTO.getId(), HttpMethod.GET, null, String.class);
        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testDeposit() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(1000.0));

        ResponseEntity<OperationDTO> postResponse = restTemplate.postForEntity(getRootApiOperationUrl() + "/deposit", operationDTO, OperationDTO.class);
        Assertions.assertNotNull(postResponse);
        Assertions.assertNotNull(postResponse.getBody());
        Assertions.assertNotNull(postResponse.getBody().getId());
        Assertions.assertEquals(postResponse.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void testWithDrawal_Ok() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(1000.0));

        ResponseEntity<OperationDTO> postResponse = restTemplate.postForEntity(getRootApiOperationUrl() + "/withdrawal", operationDTO, OperationDTO.class);
        Assertions.assertNotNull(postResponse);
        Assertions.assertNotNull(postResponse.getBody());
        Assertions.assertNotNull(postResponse.getBody().getId());
        Assertions.assertEquals(postResponse.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void testWithDrawal_Not_OK() throws JsonProcessingException {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setAccountId(accountDTO.getId());
        operationDTO.setAmount(BigDecimal.valueOf(11000.0));

        ResponseEntity<String> response = restTemplate.postForEntity(getRootApiOperationUrl() + "/withdrawal", operationDTO, String.class);
        Assertions.assertNotNull(response);
        ObjectMapper mapper = new ObjectMapper();
        var data = mapper.readValue(response.getBody(), Map.class);
        Assertions.assertNotNull(data);
        Assertions.assertEquals(data.get("errorCode"), "ERR_MCS_BALANCE_IS_INSUFFISENT");
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
