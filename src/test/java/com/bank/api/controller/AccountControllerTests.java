package com.bank.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bank.api.data.dto.AccountDTO;
import com.bank.api.data.entity.Account;
import com.bank.api.services.account.AccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class AccountControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AccountService accountService;

    private String getRootAccountApiUrl() {
        return "http://localhost:" + port + "/api/account";
    }

    @Test
    public void testCreateAccount() {
        Account account = new Account();
        account.setFirstName("John");
        account.setLastName("Doe");
        account.setBalance(BigDecimal.valueOf(5000.0));
        ResponseEntity<AccountDTO> postResponse = restTemplate.postForEntity(getRootAccountApiUrl(), account, AccountDTO.class);
        Assertions.assertNotNull(postResponse);
        Assertions.assertNotNull(postResponse.getBody());
        Assertions.assertNotNull(postResponse.getBody().getId());
        Assertions.assertEquals(postResponse.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void testGetAllAccounts() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> getResponse = restTemplate.exchange(getRootAccountApiUrl(), HttpMethod.GET, entity,
                String.class);
        Assertions.assertNotNull(getResponse);
        Assertions.assertEquals(getResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testGetAccountById_OK() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        AccountDTO resultAccountDTO = restTemplate.getForObject(getRootAccountApiUrl() + "/" + accountDTO.getId(), AccountDTO.class);
        Assertions.assertNotNull(resultAccountDTO);
        Assertions.assertNotNull(resultAccountDTO.getId());
    }

    @Test
    public void testGetAccountById_NOT_FOUND() throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> getResponse = restTemplate.exchange(getRootAccountApiUrl() + "/123456789", HttpMethod.GET, entity,
                String.class);
        Assertions.assertNotNull(getResponse);
        ObjectMapper mapper = new ObjectMapper();
        var data = mapper.readValue(getResponse.getBody(), Map.class);
        Assertions.assertNotNull(data);
        Assertions.assertEquals(data.get("errorCode"), "ERR_ACCOUNT_IS_NOT_FOUND");
        Assertions.assertEquals(getResponse.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @Test
    public void deleteAccount_exist() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setBalance(BigDecimal.valueOf(4000.0));
        accountDTO = accountService.create(accountDTO);

        ResponseEntity<Boolean> response = restTemplate.exchange(getRootAccountApiUrl() + "/" + accountDTO.getId(), HttpMethod.DELETE, null,
                Boolean.class);
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void deleteAccount_not_exist() throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootAccountApiUrl() + "/123456789", HttpMethod.DELETE, entity,
                String.class);
        Assertions.assertNotNull(response);
        ObjectMapper mapper = new ObjectMapper();
        var data = mapper.readValue(response.getBody(), Map.class);
        Assertions.assertNotNull(data);
        Assertions.assertEquals(data.get("errorCode"), "ERR_ACCOUNT_IS_NOT_FOUND");
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
