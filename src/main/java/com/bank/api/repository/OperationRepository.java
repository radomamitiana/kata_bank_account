package com.bank.api.repository;

import com.bank.api.data.entity.Account;
import com.bank.api.data.entity.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
    List<Operation> findOperationByAccount(Account account);
}
