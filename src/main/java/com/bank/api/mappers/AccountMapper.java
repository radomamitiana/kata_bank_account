package com.bank.api.mappers;

import com.bank.api.data.dto.AccountDTO;
import com.bank.api.data.entity.Account;

public interface AccountMapper {
    default Account accountDTOtoAccount(AccountDTO accountDTO) {
        Account account = new Account();
        account.setBalance(accountDTO.getBalance());
        account.setFirstName(accountDTO.getFirstName());
        account.setLastName(accountDTO.getLastName());
        return account;
    }

    default AccountDTO accountToAccountDTO(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setBalance(account.getBalance());
        accountDTO.setFirstName(account.getFirstName());
        accountDTO.setLastName(account.getLastName());
        return accountDTO;
    }
}
