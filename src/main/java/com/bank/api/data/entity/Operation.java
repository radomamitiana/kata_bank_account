package com.bank.api.data.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Operation {
    @Id
    @GeneratedValue
    private Long id;
    private String type;
    private BigDecimal amount;
    private LocalDate date;

    @ManyToOne
    private Account account;
}
