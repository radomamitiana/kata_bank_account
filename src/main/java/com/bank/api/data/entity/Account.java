package com.bank.api.data.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String LastName;
    private String firstName;
    private BigDecimal balance;
    private LocalDate createdAt;
    private LocalDate updatedAt;

    @OneToMany(mappedBy = "account")
    private Set<Operation> operations = new HashSet<>();
}
