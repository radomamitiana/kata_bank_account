package com.bank.api.data.dto;

import com.bank.api.data.dto.common.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class AccountDTO extends BaseDTO {
    private Long id;
    private String LastName;
    private String firstName;
    private BigDecimal balance;
}
