package com.bank.api.data.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistoryDTO {
    private Long operationId;
    private String lastName;
    private String firstName;
    private String type;
    private BigDecimal amount;
    private LocalDate localDate;
}
