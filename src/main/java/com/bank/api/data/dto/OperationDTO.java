package com.bank.api.data.dto;

import com.bank.api.data.dto.common.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class OperationDTO extends BaseDTO {
    private Long id;
    private BigDecimal amount;
    private Long accountId;
    private LocalDate date;
}
