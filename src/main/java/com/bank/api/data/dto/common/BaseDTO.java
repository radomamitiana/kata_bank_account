package com.bank.api.data.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public abstract class BaseDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private boolean isError = false;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private boolean isInfo = false;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private boolean isWarning = false;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String uuid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorCode = null;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorMessage = null;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message = null;

    public BaseDTO(boolean isError, String errorCode, String errorMessage, String message) {
        super();
        this.isError = isError;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.message = message;
    }

    public BaseDTO(String uuid, String errorMessage, String message) {
        this.uuid = uuid;
        this.errorMessage = errorMessage;
        this.message = message;
    }
}
