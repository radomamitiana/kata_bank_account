package com.bank.api.controller;

import com.bank.api.data.dto.AccountDTO;
import com.bank.api.services.account.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountDTO> getAccountById(@PathVariable(value = "id") Long accountId) {
        return ResponseEntity.ok().body(accountService.getAccountById(accountId));
    }

    @PostMapping("")
    public ResponseEntity<AccountDTO> create(@RequestBody AccountDTO accountDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(accountService.create(accountDTO));
    }

    @GetMapping("")
    public ResponseEntity<List<AccountDTO>> getAllAccounts() {
        return ResponseEntity.ok().body(accountService.getAllAccount());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long accountId) {
        return ResponseEntity.ok().body(accountService.deleteAccount(accountId));
    }
}
