package com.bank.api.controller;

import com.bank.api.data.dto.HistoryDTO;
import com.bank.api.data.dto.OperationDTO;
import com.bank.api.services.operation.OperationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/operation")
public class OperationController {

    private final OperationService operationService;

    public OperationController(OperationService operationService) {
        this.operationService = operationService;
    }

    @PostMapping("/deposit")
    public ResponseEntity<OperationDTO> deposit(@RequestBody OperationDTO operationDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(operationService.deposit(operationDTO));
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<OperationDTO> withdrawal(@RequestBody OperationDTO operationDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(operationService.withdrawal(operationDTO));
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<List<HistoryDTO>> getMyOperations(@PathVariable("accountId") Long accountId) {
        return ResponseEntity.ok().body(operationService.getMyOperations(accountId));
    }
}
