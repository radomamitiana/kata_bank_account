package com.bank.api.controller;

import java.util.UUID;

import com.bank.api.data.dto.common.BaseDTO;
import com.bank.api.exceptions.ErrorsEnum;
import com.bank.api.exceptions.FunctionalException;
import com.bank.api.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(FunctionalException.class)
    public final ResponseEntity<BaseDTO> handleFunctionalException(FunctionalException e, WebRequest request) {
        String uuid = UUID.randomUUID().toString();
        log.error("Functional Error ID {} ", uuid, e);
        BaseDTO baseDTO = e.getBaseDTO();
        ErrorsEnum errorsEnum = e.getErrorsEnum();
        log.error("Error message : {} ", errorsEnum.getErrorMessage());

        baseDTO.setError(true);
        baseDTO.setUuid(uuid);
        baseDTO.setErrorMessage(errorsEnum.getErrorMessage());
        baseDTO.setErrorCode(errorsEnum.getErrorCode());

        return new ResponseEntity<>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(TechnicalException.class)
    public final ResponseEntity<BaseDTO> handleTechnicalException(TechnicalException e, WebRequest request) {
        String uuid = UUID.randomUUID().toString();
        log.error("Technical Error ID {} ", uuid, e);
        BaseDTO baseDTO = e.getBaseDTO();
        ErrorsEnum errorsEnum = e.getErrorsEnum();
        log.error("Error message : {} ", errorsEnum.getErrorMessage());

        baseDTO.setError(true);
        baseDTO.setUuid(uuid);
        baseDTO.setErrorMessage(errorsEnum.getErrorMessage());
        baseDTO.setErrorCode(errorsEnum.getErrorCode());

        return new ResponseEntity<>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}