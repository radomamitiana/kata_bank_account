package com.bank.api.services.account;

import com.bank.api.data.dto.AccountDTO;

import java.util.List;

public interface AccountService {
    AccountDTO create(AccountDTO account);

    AccountDTO getAccountById(Long id);

    List<AccountDTO> getAllAccount();

    Boolean deleteAccount(Long id);
}