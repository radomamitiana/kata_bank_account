package com.bank.api.services.account;

import com.bank.api.exceptions.ErrorsEnum;
import com.bank.api.exceptions.ObjectNotFoundException;
import com.bank.api.mappers.AccountMapper;
import com.bank.api.repository.AccountRepository;
import com.bank.api.data.dto.AccountDTO;
import com.bank.api.data.entity.Account;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService, AccountMapper {

    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public AccountDTO create(AccountDTO account) {
        return accountToAccountDTO(this.accountRepository.save(accountDTOtoAccount(account)));
    }

    public AccountDTO getAccountById(Long id) {
        Account account = this.accountRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(new AccountDTO(), ErrorsEnum.ERR_MCS_NEW_PASSWORD_IS_EMPTY));
        return accountToAccountDTO(account);
    }

    public List<AccountDTO> getAllAccount() {
        return this.accountRepository.findAll()
                .stream()
                .map(this::accountToAccountDTO)
                .collect(Collectors.toList());
    }

    public Boolean deleteAccount(Long id) {
        Account account = this.accountRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(new AccountDTO(), ErrorsEnum.ERR_MCS_NEW_PASSWORD_IS_EMPTY));
        this.accountRepository.delete(account);
        return Boolean.TRUE;
    }
}