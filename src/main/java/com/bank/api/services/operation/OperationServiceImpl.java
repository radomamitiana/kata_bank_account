package com.bank.api.services.operation;

import com.bank.api.exceptions.ErrorsEnum;
import com.bank.api.exceptions.FunctionalInvalidDataException;
import com.bank.api.exceptions.ObjectNotFoundException;
import com.bank.api.repository.AccountRepository;
import com.bank.api.repository.OperationRepository;
import com.bank.api.data.dto.AccountDTO;
import com.bank.api.data.dto.HistoryDTO;
import com.bank.api.data.dto.OperationDTO;
import com.bank.api.data.entity.Account;
import com.bank.api.data.entity.Operation;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OperationServiceImpl implements OperationService {

    private final AccountRepository accountRepository;
    private final OperationRepository operationRepository;

    public OperationServiceImpl(AccountRepository accountRepository, OperationRepository operationRepository) {
        this.accountRepository = accountRepository;
        this.operationRepository = operationRepository;
    }

    @Override
    public OperationDTO deposit(OperationDTO operationDTO) {
        Account account = getAccount(operationDTO.getAccountId());
        Operation operation = new Operation();
        operation.setAccount(account);
        operation.setType("DEPOSIT");
        operation.setDate(LocalDate.now());
        operation.setAmount(operationDTO.getAmount());

        BigDecimal balance = account.getBalance().add(operationDTO.getAmount()) ;
        account.setBalance(balance);

        this.accountRepository.save(account);
        operation = this.operationRepository.save(operation);
        operationDTO.setId(operation.getId());
        return operationDTO;
    }

    @Override
    public OperationDTO withdrawal(OperationDTO operationDTO) {
        Account account = getAccount(operationDTO.getAccountId());
        Operation operation = new Operation();
        operation.setAccount(account);
        operation.setType("WITH_DRAWAL");
        operation.setDate(LocalDate.now());
        operation.setAmount(operationDTO.getAmount());

        if (account.getBalance().compareTo(operationDTO.getAmount()) < 0 ) {
            throw new FunctionalInvalidDataException(new OperationDTO(), ErrorsEnum.ERR_MCS_BALANCE_IS_INSUFFISENT);
        }
        BigDecimal balance = account.getBalance().subtract(operationDTO.getAmount());
        account.setBalance(balance);
        this.accountRepository.save(account);
        operation = this.operationRepository.save(operation);
        operationDTO.setId(operation.getId());
        return operationDTO;
    }

    @Override
    public List<HistoryDTO> getMyOperations(Long accountId) {
        Account account = getAccount(accountId);
        List<Operation> operations = operationRepository.findOperationByAccount(account);
        return operations
                .stream()
                .map(operation -> mapToHistoryDTO(operation, account))
                .collect(Collectors.toList());
    }

    private HistoryDTO mapToHistoryDTO(Operation operation, Account account) {
        HistoryDTO historyDTO = new HistoryDTO();
        historyDTO.setOperationId(operation.getId());
        historyDTO.setAmount(operation.getAmount());
        historyDTO.setFirstName(account.getFirstName());
        historyDTO.setLastName(account.getLastName());
        historyDTO.setLocalDate(operation.getDate());
        return historyDTO;
    }

    private Account getAccount(Long accountId) {
        return this.accountRepository.findById(accountId)
                .orElseThrow(() -> new ObjectNotFoundException(new AccountDTO(), ErrorsEnum.ERR_MCS_NEW_PASSWORD_IS_EMPTY));
    }
}
