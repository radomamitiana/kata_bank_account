package com.bank.api.services.operation;

import com.bank.api.data.dto.HistoryDTO;
import com.bank.api.data.dto.OperationDTO;

import java.util.List;

public interface OperationService {
    OperationDTO deposit(OperationDTO operationDTO);

    OperationDTO withdrawal(OperationDTO operationDTO);

    List<HistoryDTO> getMyOperations(Long accountId);
}

