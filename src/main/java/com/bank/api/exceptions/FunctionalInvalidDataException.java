package com.bank.api.exceptions;

import com.bank.api.data.dto.common.BaseDTO;

public class FunctionalInvalidDataException extends FunctionalException{
	private static final long serialVersionUID = 1L;
	

	public FunctionalInvalidDataException(BaseDTO baseDTO, ErrorsEnum errorsEnum, Exception e) {
		super(baseDTO, errorsEnum, e);
	}
	
	public FunctionalInvalidDataException(BaseDTO baseDTO, ErrorsEnum errorsEnum) {
		super(baseDTO, errorsEnum);
	}
	
}
