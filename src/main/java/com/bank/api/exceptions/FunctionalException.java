package com.bank.api.exceptions;

import com.bank.api.data.dto.common.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FunctionalException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private BaseDTO baseDTO;
    private ErrorsEnum errorsEnum;

    public FunctionalException(BaseDTO baseDTO, ErrorsEnum errorsEnum, Exception e) {
        super(e);
        this.baseDTO = baseDTO;
        this.errorsEnum = errorsEnum;
    }
}
