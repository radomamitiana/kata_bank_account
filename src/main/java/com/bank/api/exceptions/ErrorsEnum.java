package com.bank.api.exceptions;

import java.util.HashMap;
import java.util.Map;

public enum ErrorsEnum {
    ERR_MCS_NEW_PASSWORD_IS_EMPTY("ERR_ACCOUNT_IS_NOT_FOUND", "Account is not found"),
    ERR_MCS_BALANCE_IS_INSUFFISENT("ERR_MCS_BALANCE_IS_INSUFFISENT", "Account is insuffisent");

    private final String errorCode;
    private final String errorMessage;

    private static final Map<String, ErrorsEnum> BYID = new HashMap<>();

    static {
        for (ErrorsEnum e : ErrorsEnum.values()) {
            if (BYID.put(e.getErrorCode(), e) != null) {
                throw new IllegalArgumentException("duplicate id: " + e.getErrorCode());
            }
        }
    }

    private ErrorsEnum(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "ErrorCode : " + errorCode + " errorMessage : " + errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public static ErrorsEnum getById(String id) {
        return BYID.get(id);
    }
}
