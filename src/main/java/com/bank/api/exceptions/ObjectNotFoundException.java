package com.bank.api.exceptions;

import com.bank.api.data.dto.common.BaseDTO;

public class ObjectNotFoundException extends TechnicalException {
	private static final long serialVersionUID = 1L;

	public ObjectNotFoundException(BaseDTO baseDTO, ErrorsEnum errorsEnum, Exception e) {
		super(baseDTO, errorsEnum, e);
	}

	public ObjectNotFoundException(BaseDTO baseDTO, ErrorsEnum errorsEnum) {
		super(baseDTO, errorsEnum);
	}

}
