A bank Account API
* Deposit and Withdrawal
* Account statement (date, amount, balance)
* Statement printing
The expected result is a service API, and its underlying implementation, that meets the expressed needs.

Technicals environments: SPRING BOOT, SPRING DATA, Hibernate, JUnit, H2 Database, Java 11, Lombok

NOTES: run the following command to build  the project and run all unit test: mvn clean install